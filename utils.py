import numpy as np
from matplotlib import pyplot as plt
from skimage.transform import resize
from glob import glob
import json
from pathlib import Path
import os

def compute_rayleigh_scale(noise_fn='noise.png'):
    noise = Path(noise_fn)
    if not noise.exists():
        scale = 1
    else:
        noise = plt.imread(noise_fn)
        mean = np.mean(noise.flatten())
        scale = mean / np.sqrt(np.pi / 2.)
    return scale

def get_free_position(shape, positions,
       size=(1783, 1477) ,override=True, max_iter=300):
    for i in range(max_iter):
        print('iteration {}'.format(i))
        x_tl = np.random.randint(0, size[0]-shape[0])
        y_tl = np.random.randint(0, size[1]-shape[1])
        x_br = x_tl + shape[0]
        y_br = y_tl + shape[1]
        if override or not batch_intersect(positions, [x_tl, y_tl, x_br, y_br]):
            return [x_tl, y_tl, x_br, y_br]
    return None
        
def batch_intersect(batch_positions, batch):
    for pos in batch_positions:
        if not (batch[1]>pos[3] or batch[0]>pos[2] or
                batch[3]<pos[1] or batch[2]<pos[0]):
            return True
    return False

def insert_batch_in_img(batch, img, pos=[0,0]):
    b_h, b_w = batch.shape[0], batch.shape[1]
    img[pos[0]:pos[0]+b_h, pos[1]:pos[1]+b_w] = batch
    return img

def create_simulated_img(targets, background, shape=(1783, 1477)):
    # Create output images and fill them with background areas
    # and targets
    dataset_dir = '/home/idriss/MSTAR/'
    height, width = shape
    scale = compute_rayleigh_scale()

    # create a rayleigh noise image
    img = np.random.rayleigh(scale=scale, size=shape)

    # grab and insert a random number of batches randomly
    n_batches = np.random.randint(0, len(background))
    print('I will insert {} batches in the image'.format(n_batches))
    batches = np.random.choice(background, size=n_batches)

    # grab and insert a random number of targets randomly
    n_targets = np.random.randint(7, 30)
    print('I will insert {} targets in the image'.format(n_targets))
    rand_targets = np.random.choice(targets, size=n_targets)

    out_targets = {
            'height': shape[0],
            'width': shape[1]
        }

    img, target_positions = insert_portions(
            img, targets=rand_targets, batches=batches, size=shape, scale=scale)

    out_targets['tlx'] = target_positions['tlx']
    out_targets['tly'] = target_positions['tly']
    out_targets['brx'] = target_positions['brx']
    out_targets['bry'] = target_positions['bry']
    out_targets['label'] = target_positions['label']
    
    return img, out_targets

def insert_portions(
        img, scale, targets=None, batches=None, size=(1783, 1477)):

    b_positions = []
    # put the batches in the image
    for batch in batches :
        position = get_free_position(
                batch.shape,
                b_positions,
                size=size,
                override=True)

        if position is not None:
            batch_scale = batch.mean() / np.sqrt(np.pi / 2.)
            batch = batch * scale / batch_scale
            img = insert_batch_in_img(batch, img, position)
            b_positions.append(position)

    # define the output target positions dictionary
    output_positions = {
            'tlx': [],
            'tly': [],
            'brx': [],
            'bry': [],
            'label': []
            }
    t_positions = []
    # put the targets in the image
    for target in targets :
        position = get_free_position(
                target['img'].shape,
                t_positions,
                size=size,
                override=False)

        if position is not None:
            target_scale = target['img'].mean() / np.sqrt(np.pi / 2.)
            target['img'] = target['img'] * scale / target_scale
            img = insert_batch_in_img(target['img'], img, position)
            t_positions.append(position)
            output_positions['tlx'].append(int(position[0]+target['coords'][0]))
            output_positions['tly'].append(int(position[1]+target['coords'][1]))
            output_positions['brx'].append(int(position[0]+target['coords'][2]))
            output_positions['bry'].append(int(position[1]+target['coords'][3]))
            output_positions['label'].append(target['label'])

    return img, output_positions

