#!/usr/bin/env python
# coding: utf-8

# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import numpy as np
from skimage import color
from matplotlib import pyplot as plt
from skimage.transform import resize
from glob import glob
import json
from pathlib import Path
import os
import tensorflow as tf
from object_detection.utils import dataset_util
from collections import Counter
import matplotlib
from matplotlib.font_manager import FontProperties
import matplotlib as mpl 
import codecs
import hashlib
import io
import PIL.Image
import argparse

from tfrecords_conversion import *
from utils import create_simulated_img

def parse_args():
    parser = argparse.ArgumentParser(description='Simulate an Object Detection dataset.')

    parser.add_argument('--override',
            type=lambda x: (str(x).lower() == 'true'), default=True,
                        help='re-create the dataset or just regenerate the\
                        tfrecords from the existing data (default: True)')
    parser.add_argument('--target_size', type=int, default=200,
                        help='target image size (default: 200)')
    parser.add_argument('--count', type=int, default=500,
                        help='sum the integers (default: 500)')
    parser.add_argument('--min_imgs', type=int, default=5,
                        help='minimum number of target images to be\
                                considered (default: 5)')
    parser.add_argument('--shape', type=int, default=[1783, 1477], nargs=2,
                        help='the shape of the output image (default: 1783x1477)')
    parser.add_argument('--target-ignore',
            type=lambda x: x.split(',') ,default='SLICY',
            help='Comma separated list of targets to ignore, "brdm, zsu, slicy, 2s1"\
                    (default: SLICY)')

    return parser.parse_args()

def should_be_ignored(target_ignore, path_name):
    for target in target_ignore:
        if target.lower() in path_name.lower():
            return True
    return False

def main():
    args = parse_args()

    # define some constants
    target_size = args.target_size
    override = args.override
    n_output_imgs = args.count
    out_height, out_width = args.shape 
    noise_fn = 'noise.png'
    dataset_dir = '/home/idriss/MSTAR/'
    output_dir = 'data'
    min_train = args.min_imgs
    mpl.rcParams['figure.dpi']= 250
    zh_font = FontProperties(
            fname = '/usr/share/fonts/opentype/noto/NotoSerifCJK-Regular.ttc')

    # Check whether data directory exists
    data = Path('data')
    if not data.exists():
        data.mkdir()

    # read old annotations
    annotation_fn = 'annot.json'
    with open(annotation_fn, 'r') as f:
        annotations = json.load(f)

    # read and store target regions we want the model to detect
    targets = []
    for a in annotations:
        if  not should_be_ignored(args.target_ignore, a['path']):
            img_path = os.path.join(dataset_dir, a['path'])
            img = plt.imread(img_path)
            img = color.rgb2gray(img)
            img = resize(img, (target_size,target_size), anti_aliasing=True)
            coords = np.array([a['tlx'], a['tly'], a['brx'], a['bry']]) * target_size // 54
            targets.append({'img': img, 'label': a['label'], 'path': img_path, 
                'coords': coords})

    # read and store regions we want the model to ignore 
    ignore_fns = glob('ignore/*.png')
    background = []
    for fn in ignore_fns :
        bg_img = plt.imread(fn)
        bg_img = color.rgb2gray(bg_img)
        background.append(bg_img)

    # Create output images and fill them with background portions
    # and targets
    all_targets = []
    mt_annotation = Path('mt_annotations.json')
    if override or not mt_annotation.exists(): 
        for i in range(n_output_imgs):
            img, out_targets = create_simulated_img(
                    targets, background, shape=(out_height, out_width))
            plt.figure()
            img_path = 'data/{}.png'.format(i)
            plt.imsave(img_path, img)
            out_targets['path'] = img_path
            all_targets.append(out_targets)

        # save the output annotations
        with open('mt_annotations.json', 'w') as f:
            json.dump(all_targets, f, indent=4)
    else:
        with open('mt_annotations.json', 'r') as f:
            all_targets = json.load(f)

    # create the tfrecords
    # Check whether tfrecords directory exists, create it otherwise
    data = Path('tfrecords')
    if not data.exists():
        data.mkdir()

    # First show statistics
    target_names, n_imgs = get_names_numbers(all_targets)
    labels, values = show_statistics(target_names)
    saved_dict, reverse_dict = create_the_label_map(values, labels)

    create_tfrecords('tfrecords', all_targets, saved_dict)

if __name__ == '__main__':
    main()

